#!/usr/bin/env python

def calculate(input_file):
  series = 0
  past = set()
  iteration = 0
  while True:
    print("Iteration %d, frequency %d" % (iteration, series))
    for string_delta in input_file:
      delta = int(string_delta)
      series += delta

      old_count = len(past)
      past.add(series)
      if len(past) == old_count:
        return series

    iteration += 1
    input_file.seek(0)

with open("input.txt", "r") as input_file:
  print("The first frequeny that is reached twice is: " +
        str(calculate(input_file)))
