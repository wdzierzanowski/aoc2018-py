#!/usr/bin/env python

def calculate(input_file):
  series = 0
  for string_delta in input_file:
    delta = int(string_delta)
    series += delta
  return series

with open("input.txt", "r") as input_file:
  print("The series is: " + str(calculate(input_file)))
