#!/usr/bin/env python

def has_letter_repeated_times(string, expected_times):
  sorted_string = sorted(string)
  times = 1
  for i in range(len(sorted_string) - 1):
    if sorted_string[i] == sorted_string[i + 1]:
      times += 1
    else:
      if times == expected_times:
        return True
      times = 1
  return times == expected_times

def calculate(input_file):
  twos_count = 0
  threes_count = 0
  for box_id in input_file:
    box_id = box_id.strip()
    if has_letter_repeated_times(box_id, 2):
      print(box_id + " has a latter repeated 2 times")
      twos_count += 1
    if has_letter_repeated_times(box_id, 3):
      print(box_id + " has a latter repeated 3 times")
      threes_count += 1
  return twos_count * threes_count

with open("input.txt", "r") as input_file:
  print("The checksum is: " + str(calculate(input_file)))
