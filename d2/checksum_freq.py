#!/usr/bin/env python

def get_frequencies(string):
  frequencies = {}
  for c in string:
    if c not in frequencies.keys():
      frequencies[c] = 1
    else:
      frequencies[c] += 1
  return frequencies

def calculate(input_file):
  twos_count = 0
  threes_count = 0
  for box_id in input_file:
    box_id = box_id.strip()
    frequencies = get_frequencies(box_id)
    if 2 in frequencies.values():
      print(box_id + " has a latter repeated 2 times")
      twos_count += 1
    if 3 in frequencies.values():
      print(box_id + " has a latter repeated 3 times")
      threes_count += 1
  return twos_count * threes_count

with open("input.txt", "r") as input_file:
  print("The checksum is: " + str(calculate(input_file)))
