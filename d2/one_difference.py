#!/usr/bin/env python

def get_common_letters(a, b):
  assert(len(a) == len(b))
  letters = [a[i] if a[i] == b[i] else '' for i in range(len(a))]
  return "".join(letters)

def calculate(input_file):
  box_ids = [box_id.strip() for box_id in input_file]
  for a in box_ids:
    for b in box_ids:
      common = get_common_letters(a, b)
      if len(common) == len(a) - 1:
        return common
  assert False

with open("input.txt", "r") as input_file:
  print("The common letters are: " + calculate(input_file))
