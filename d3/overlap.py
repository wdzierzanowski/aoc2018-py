#!/usr/bin/env python

from collections import defaultdict
import re

def parse_claim_spec(spec):
  """Parse a fabric claim specification line of the form:
       #123 @ 3,2: 5x4
     into (left, top, width, height) = (3, 2, 5, 4)
  """
  return map(int, re.findall(r"\d+", spec))

def count_claims(input_file):
  claims = defaultdict(list)
  overlaps = {}
  for claim_spec in input_file:
    (claim_id, left, top, width, height) = parse_claim_spec(claim_spec)
    overlaps[claim_id] = set()
    for x in range(left, left + width):
      for y in range(top, top + height):
        for overlapped_id in claims[(x, y)]:
          overlaps[claim_id].add(overlapped_id)
          overlaps[overlapped_id].add(claim_id)
        claims[(x, y)].append(claim_id)
  return (claims, overlaps)

def calculate_overlaps(cells):
  overlap = 0
  for claims in cells.values():
    if len(claims) >= 2:
      overlap += 1
  return overlap

def get_nonoverlapped_claim(overlaps):
  nonoverlapped = list(filter(lambda claim_id: len(overlaps[claim_id]) == 0,
                              overlaps))
  assert(len(nonoverlapped) == 1)
  return nonoverlapped[0]

with open("input.txt", "r") as input_file:
  (claims, overlaps) = count_claims(input_file)
  print("Square inches within two or more claims: " +
        str(calculate_overlaps(claims)))
  print("ID of the only claim that doesn't overlap: " +
        str(get_nonoverlapped_claim(overlaps)))
