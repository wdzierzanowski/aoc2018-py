#!/usr/bin/env python

from collections import defaultdict
from datetime import datetime
import functools
import re

def parse_repose_records(input_file):
  """Returns [(timestamp, event), ...] sorted by timestamp"""
  expr = re.compile(r"^\[([^\]]+)\] (.*)$")
  parsed_entries = [re.match(expr, entry).groups() for entry in input_file]
  datetime_parser = \
      lambda entry: (datetime.strptime(entry[0], "%Y-%m-%d %H:%M"), entry[1])
  parsed_entries = map(datetime_parser, parsed_entries)
  sorted_entries = sorted(parsed_entries, key = lambda entry: entry[0])
  return sorted_entries

def count_repose_times(entries):
  """Returns {guard_id -> [(sleep_start, awake_start), ...])}"""
  guards = defaultdict(list)
  guard_id = None
  sleep_start = None
  guard_id_expr = re.compile(r"\d+")
  for entry in entries:
    if entry[1] == "falls asleep":
      sleep_start = entry[0].minute
    elif entry[1] == "wakes up":
      awake_start = entry[0].minute
      interval = (sleep_start, awake_start)
      guards[guard_id].append(interval)
    else:
      guard_id = int(guard_id_expr.findall(entry[1])[0])
  guards[guard_id].append(interval)
  return guards

def get_max_sleeper(guards):
  """Returns (guard_id, [(sleep_start, awake_start), ...])""" 
  sum_of_intervals = lambda acc, interval: acc + interval[1] - interval[0]
  keyfun = lambda guard: functools.reduce(sum_of_intervals, guard[1], 0)
  return max(guards.items(), key = keyfun)

def get_most_slept_minute(intervals):
  """Returns (minute, times_slept)"""
  minutes = defaultdict(int)
  for interval in intervals:
    for m in range(interval[0], interval[1]):
      minutes[m] += 1
  return max(minutes.items(), key = lambda i: i[1])

def get_most_slept_minute_overall(guards):
  """Returns (guard_id, minute)"""
  guard = max(guards.items(), key = lambda g: get_most_slept_minute(g[1])[1])
  return (guard[0], get_most_slept_minute(guard[1])[0])

with open("input.txt", "r") as input_file:
  entries = parse_repose_records(input_file)
  guards = count_repose_times(entries)
  print(guards)
  guard = get_max_sleeper(guards)
  print(guard)
  most_slept_minute = get_most_slept_minute(guard[1])[0]
  print(most_slept_minute)
  print(guard[0] * most_slept_minute)
  
  (guard_id, minute) = get_most_slept_minute_overall(guards)
  print("Guard " + str(guard_id) + " spent minute " + str(minute) \
        + " asleep more than any other guard or minute.")
  print(guard_id * minute)
