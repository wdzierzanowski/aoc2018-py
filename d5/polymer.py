#!/usr/bin/env python

import string

def is_reaction(x, y):
  return abs(ord(x) - ord(y)) == 32

def react(polymer):
  """Returns the fully reacted polymer"""
  reacted = []
  for unit in polymer:
    if reacted and is_reaction(unit, reacted[-1]):
      reacted.pop()
    else:
      reacted.append(unit)
  return reacted

def filtered(polymer, unit):
  assert(unit.islower())
  return filter(lambda x: x.lower() != unit, polymer)

def optimize(polymer):
  """Returns the length of the polymer reacted optimally"""
  filtered_lengths = map(lambda unit: len(react(filtered(polymer, unit))),\
                         string.ascii_lowercase)
  return min(filtered_lengths)

with open("input.txt", "r") as input_file:
  polymer = input_file.read()[0:-1]
  reacted = react(polymer)
  print(len(reacted))

  optimized = optimize(polymer)
  print(optimized)
