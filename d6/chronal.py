#!/usr/bin/env python

from collections import defaultdict
from collections import namedtuple

Point = namedtuple('Point', ['x', 'y'])
Rect = namedtuple('Rect', ['topleft', 'bottomright'])

def parse_coords(lines):
  return map(lambda line: Point._make(map(int, line.split(','))), lines)

def get_distance(location0, location1):
  return abs(location0.x - location1.x) + abs(location0.y - location1.y)

def get_closest_coord(location, coords):
  coord_distances = list(map(lambda c: (c, get_distance(location, c)), coords))
  distances = list(map(lambda cd: cd[1], coord_distances))
  min_distance_coord = min(coord_distances, key = lambda cd: cd[1])
  return min_distance_coord[0] if distances.count(min_distance_coord[1]) == 1 \
         else None

def get_grid_rect(coords):
  min_x = min(coords, key = lambda c: c.x).x
  min_y = min(coords, key = lambda c: c.y).y
  max_x = max(coords, key = lambda c: c.x).x
  max_y = max(coords, key = lambda c: c.y).y
  return Rect(Point(min_x, min_y), Point(max_x, max_y))

def is_on_edge(coord, grid):
  return coord.x == grid.topleft.x \
         or  coord.x == grid.bottomright.x \
         or coord.y == grid.topleft.y \
         or coord.y == grid.bottomright.y

def calculate_area(coord, coords):
  """Calculate the number of locations that are closest to |coord|."""
  grid = get_grid_rect(coords)
  neighbors = [coord]
  area = [coord]
  while len(neighbors) > 0:
    #print("Neighbors: " + str(neighbors) + "\nCoord: " + str(coord) + \
    #      "\nArea: " + str(area))
    location = neighbors.pop()
    for x in range(location.x - 1, location.x + 2):
      for y in range(location.y - 1, location.y + 2):
        neighbor = Point(x, y)
        if neighbor != location and neighbor not in neighbors \
            and neighbor not in area:
          if get_closest_coord(neighbor, coords) == coord:
            if is_on_edge(neighbor, grid):
              print(str(coord) + " is infinite")
              return None
            neighbors.append(neighbor)
            area.append(neighbor)
    #input()
  print("%s: %d\n" % (str(coord), len(area)))
  return len(area)

def grid(coords):
  grid_rect = get_grid_rect(coords)
  for x in range(grid_rect.bottomright.x):
    for y in range(grid_rect.bottomright.y):
      yield Point(x, y)

def get_distance_to_all(location, coords):
  return sum(get_distance(location, coord) for coord in coords)

with open("input.txt", "r") as input_file:
  coords = list(parse_coords(input_file))
  areas = map(lambda coord: calculate_area(coord, coords), coords)
  finite_areas = filter(lambda a: a is not None, areas)
  print(sorted(finite_areas))

  print(sum(get_distance_to_all(location, coords) < 10000 \
        for location in grid(coords)))
