#!/usr/bin/env python

from collections import namedtuple

Data = namedtuple("Data", [
  'N',  # Number of steps
  'W',  # Number of workers
  'T',  # Task |n| takes |T + n| seconds to finish
  'filename'
]);

data = Data(26, 5, 61, "input.txt")
#data = Data(6, 2, 1, "input.txt.test")

class Vertex(namedtuple('Vertex', ['v', 'predecessors'])):
  __slots__ = ()
  def __str__(self):
    return '%s %s' % (self.v, str(self.predecessors))

Assignment = namedtuple('Assignment', ['task', 'time_to_finish'])

def print_matrix(M):
  print('  ', end='')
  for i in range(len(M)):
    print(' %s ' % M[i].v, end='')
  print()
  for i in range(len(M)):
    print(M[i])

def print_assignments(second, assignments, order):
  msg = "t:" + str(second) + "  "
  for a in assignments:
    msg += "." if a.task is None else a.task
    msg += " "
  msg += "Done: " + "".join(order)
  print(msg)

def parse_deps(lines):
  """Returns the transposed adjacency matrix with a[j][i] == 1 meaning that |i|
     must be finished before |j| can begin.
  """
  A = [Vertex(chr(i + 65), [0 for j in range(data.N)]) for i in range(data.N)]
  for line in lines:
    split_line = line.split()
    j = ord(split_line[1]) - 65
    i = ord(split_line[7]) - 65
    A[i].predecessors[j] = 1
  return A

def calculate_time_to_complete(A):
  order = []
  assignments = [Assignment(None, -1) for i in range(data.W)]
  second = 0
  while len(A) > 0:
    # Assign new tasks:
    #  - for each free worker W
    #      find first unassigned available task T
    #      assign T to W
    next_indices = (j for j in range(len(A)) \
                      if sum(A[j].predecessors) == 0 and \
                      A[j].v not in [a.task for a in assignments])
    for i, a in enumerate(assignments):
      if a.task is None:
        available = next(next_indices, None)
        if available is None:
          break
        task = A[available].v
        assignments[i] = Assignment(task, ord(task) - 65 + data.T)

    print_assignments(second, assignments, order)

    # Advance time:
    #  - decrement time to finish for each ongoing task
    #  - remove finished tasks from A
    assignments = [Assignment(a.task, max(a.time_to_finish - 1, 0))
                   for a in assignments]

    for i, a in enumerate(assignments):
      if a.task is not None and a.time_to_finish == 0:
        assignments[i] = Assignment(None, -1)
        order.append(a.task)
        index = next(j for j in range(len(A)) if A[j].v == a.task)
        del A[index]
        for j in range(len(A)):
          del A[j].predecessors[index]
    print_matrix(A)

    second += 1
  return second, "".join(order)

with open(data.filename, "r") as input_file:
  A = parse_deps(input_file)
  print_matrix(A)

  print(calculate_time_to_complete(A))
