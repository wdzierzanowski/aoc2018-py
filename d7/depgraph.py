#!/usr/bin/env python

from collections import namedtuple

N = 26

class Vertex(namedtuple('Vertex', ['v', 'predecessors'])):
  __slots__ = ()
  def __str__(self):
    return '%s %s' % (self.v, str(self.predecessors))

def print_matrix(M):
  print('  ', end='')
  for i in range(len(M)):
    print(' %s ' % M[i].v, end='')
  print()
  for i in range(len(M)):
    print(M[i])

def parse_deps(lines):
  """Returns the transposed adjacency matrix with a[j][i] == 1 meaning that |i|
     must be finished before |j| can begin.
  """
  A = [Vertex(chr(i + 65), [0 for j in range(N)]) for i in range(N)]
  for line in lines:
    split_line = line.split()
    j = ord(split_line[1]) - 65
    i = ord(split_line[7]) - 65
    A[i].predecessors[j] = 1
  return A

def find_order(A):
  order = []
  while len(A) > 0:
    ready = next(i for i in range(len(A)) if sum(A[i].predecessors) == 0)
    order.append(A[ready].v)
    del A[ready]
    for i in range(len(A)):
      del A[i].predecessors[ready]
    print_matrix(A)
  return ''.join(order)

with open("input.txt", "r") as input_file:
  A = parse_deps(input_file)
  print_matrix(A)

  print(find_order(A))
